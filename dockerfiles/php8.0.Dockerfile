FROM php:8.0.17-fpm-alpine

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
    /usr/local/bin/

RUN apk add --no-cache \
        bash~=5 \
        supervisor~=4 \
        msmtp~=1 \
        inotify-tools~=3 \
        run-parts~=4 \
        logrotate~=3 \
        iproute2~=5 \
    # https://github.com/docker-library/php/issues/240
    && apk add --no-cache --repository https://dl-cdn.alpinelinux.org/alpine/v3.13/community/ gnu-libiconv=1.15-r3 \
    && chmod +x /usr/local/bin/install-php-extensions \
    && install-php-extensions \
        amqp \
        bcmath \
        bz2 \
        calendar \
        enchant \
        exif \
        gd \
        gettext \
        gmp \
        imagick \
        imap \
        intl \
        mailparse \
        memcached \
        mongodb \
        mysqli \
        oauth \
        opcache \
        pdo_mysql \
        pspell \
        redis \
        swoole \
        tidy \
        xsl \
        yaml \
        zip

ENV MAIL_RELAYHOST="" \
    MAIL_TLS_KEYFILE="" \
    MAIL_TLS_CRTFILE="" \
    MAIL_TLS_CAFILE="" \
    PHP_POOL_D="" \
    PHP_CONTAINER_DEBUGGING=false \
    # https://github.com/docker-library/php/issues/240
    LD_PRELOAD="/usr/lib/preloadable_libiconv.so php"

COPY files/ /

WORKDIR /var/www

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/healthcheck.sh"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
