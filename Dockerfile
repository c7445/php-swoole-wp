FROM php:8.1-fpm

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
    /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions \
    && install-php-extensions \
        exif \
        bcmath \
        bz2 \
        exif \
        gd \
        imagick \
        intl \
        mysqli \
        pdo_mysql \
        igbinary \
        redis \
        openswoole \
        zip \
        mbstring
RUN apt-get update && apt-get install -y \
    git \
    zip \
    curl \
    sudo \
    unzip
COPY ./data/php.ini /usr/local/etc/php/
COPY ./data/www.conf /usr/local/etc/php-fpm.d/
COPY ./data/server.php /usr/local/etc/php/
RUN chmod +x /usr/local/etc/php/server.php
#CMD [ "php", "./usr/local/etc/php/server.php" ]
