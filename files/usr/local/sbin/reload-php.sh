#!/usr/bin/env bash

set -Eeuo pipefail

PHP_POOL_DIR=${PHP_POOL_D:-"/etc/php-fpm.d/"}

# shellcheck disable=SC2010
# shellcheck disable=SC2086
if ! (ls -1A -- $PHP_POOL_DIR | grep -q .); then
  if supervisorctl status php-fpm; then
    echo "Pool directory $PHP_POOL_DIR is empty, gracefully quitting."
    supervisorctl stop php-fpm
  fi
else
  # reload PHP-FPM
  if supervisorctl status php-fpm; then
    supervisorctl signal USR2 php-fpm
  else
    supervisorctl start php-fpm
  fi
fi


